from django import forms
from django.forms import inlineformset_factory

from toast.models import Availability, Person, Event, Game, League, Team


class CreateAccountForm(forms.Form):
    email = forms.EmailField(label='Your e-mail address')
    derbyname = forms.CharField(label='Your public/Derby name')
    password1 = forms.CharField(label='Your Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirm Your Password', widget=forms.PasswordInput)


class AvailabilityForm(forms.ModelForm):
    class Meta:
        model = Availability
        fields = [
            'person',
            'event',
            'notes',
            'nominating_league',
            'roles',
            'preferences'
        ]


class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = [
            'display_name',
            'email',
            'home_postcode',
            'affiliation',
            'facebook_profile',
            'wftda_cert_claimed_identity',
            'pronouns',
            'games_history_link',
            'emergency_contact_details',
            'opt_in_to_update_emails',
            'notes',
            'preferred_roles',
            'preferred_positions',
        ]
    email = forms.EmailField(label='Email address')


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = [
            'name',
            'date',
            'is_tournament_game_day',
            'start_time',
            'city',
            'address',
            'schedule',
            'facebook_event_url',
            'officials_group_or_event_url',
        ]
        labels = {
            'name': 'Event Name/Short Description (optional)',
        }


class CreateEventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = [
            'name',
            'host',
            'date',
            'is_tournament_game_day',
            'start_time',
            'city',
            'address',
            'schedule',
            'facebook_event_url',
            'officials_group_or_event_url',
        ]
        labels = {
            'name': 'Event Name/Short Description (optional)',
        }


InlineGamesFormSet = inlineformset_factory(
    Event,
    Game,
    fields=('home_team', 'away_team', 'game_type', 'association'),
    min_num=1,
    extra=3
)


class CreateLeagueForm(forms.ModelForm):
    class Meta:
        model = League
        fields = [
            'name',
            'region',
            'based_in_city',
            'representatives'
        ]


InlineTeamsFormSet = inlineformset_factory(
    League,
    Team,
    min_num=1,
    fields=('name', ),
    extra=5
)
