import datetime

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from django.contrib.auth import get_user_model, login, logout
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest, JsonResponse, \
    HttpResponseServerError, HttpRequest, HttpResponseForbidden
from django.urls import reverse
from django.utils import timezone

from toast.forms import CreateAccountForm, AvailabilityForm, PersonForm, EventForm, InlineGamesFormSet, CreateEventForm, \
    CreateLeagueForm, InlineTeamsFormSet
from toast.models import Person, Event, League, Availability, Game, Role, Position, Region


def index(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('home'))

    return render(request, 'toast/landing.html')


def create_account(request):
    ctx = {}

    if request.method == 'POST':
        f = CreateAccountForm(request.POST)
        if f.is_valid():
            email = f.cleaned_data['email']
            email = email.lower()
            name = f.cleaned_data['derbyname']
            pass1 = f.cleaned_data['password1']
            pass2 = f.cleaned_data['password2']
            if pass1 != pass2:
                f.add_error('password1', 'Passwords did not match!')
            else:
                # create user!
                user = get_user_model().objects.create_user(
                    email,
                    email,
                    pass1,
                    first_name=name
                )
                login(request, user)

                return HttpResponseRedirect(reverse('myprofile'))

    else:
        f = CreateAccountForm()

    ctx['create_form'] = f
    return render(request, 'toast/create_account.html', ctx)


@login_required
def current_season(request: HttpRequest):
    now = timezone.now()
    from_date = now - datetime.timedelta(days=settings.TOAST_CALENDAR_GOBACK_DAYS)

    me: Person = Person.objects.get(user=request.user)
    events = Event.season()
    if from_date:
        events = events.filter(date__gte=from_date)
    my_availabilities = me.availability_set.filter(event__in=events)
    # transform list to dict -- is this faster than just doing one DB query for every event?
    my_events_to_availabilities = {
        avail.event: avail for avail in my_availabilities
    }
    event_to_availability = {
        event: my_events_to_availabilities.get(event) for event in events
    }
    my_administered_regions = me.admin_of_regions.all()
    my_rep_teams = me.representative_of_leagues.all()
    ctx = {
        'me': me,
        'events': events,
        'event_to_avail': event_to_availability,
        'my_administered_regions': my_administered_regions,
        'my_rep_teams': my_rep_teams,

    }
    return render(request, 'toast/season.html', ctx)


@login_required
def home(request):
    return render(request, 'toast/home.html')


@login_required
def edit_availability(request, eventid):
    event: Event = get_object_or_404(Event, pk=eventid)

    if event.crew_locked:
        return HttpResponseBadRequest('The crew for this event is locked.')

    me: Person = Person.objects.get(user=request.user)
    ctx = {
        'me': me,
        'event': event,
    }

    # Get the leagues involved in the day. Needed for nominations.
    leagues_involved = event.teams_query()

    # Is there already an availability?
    avail = me.availability_set.filter(event=event)

    if request.method == 'POST':
        if avail.exists():
            f = AvailabilityForm(request.POST, instance=avail.first())
            f.fields['nominating_league'].queryset = leagues_involved
        else:
            f = AvailabilityForm(request.POST)
            f.fields['nominating_league'].queryset = leagues_involved

        if f.is_valid():
            avail = f.save()

            # Fixup: anyone who'se chosen a position preference but not the corresponding role should have the role
            # auto-selected
            roles = Role.objects.filter(position__in=avail.preferences.all()).distinct()
            for role in roles:
                avail.roles.add(role)

            return HttpResponseRedirect(reverse('current_season'))
    else:
        if avail.exists():
            # Yes. Show a bound form.
            f = AvailabilityForm(instance=avail.first())
            f.fields['nominating_league'].queryset = leagues_involved
        else:
            # No - this is a new availability for this date.
            # ENSURE I'M NOT OVERCOMMITTING.
            existing_avails = me.availability_set.filter(event__date=event.date)
            if existing_avails.exists():
                return render(request, 'toast/already_committed.html', {'event': existing_avails.first().event})

            # Show an empty form.
            # Auto-configure avail with default user preferences & roles
            f = AvailabilityForm(initial={
                "person": me,
                "event": event,
                "roles": me.preferred_roles.all(),
                "preferences": me.preferred_positions.all()
            })
            f.fields['nominating_league'].queryset = leagues_involved

    ctx['form'] = f

    return render(request, 'toast/edit_availability.html', ctx)


@login_required
def del_availability(request, availabilityid):
    a = get_object_or_404(Availability, pk=availabilityid)
    me: Person = Person.objects.get(user=request.user)
    if a.person_id != me.pk:
        return HttpResponseBadRequest('Nice try, this is not your availability')

    if a.event.crew_locked:
        return HttpResponseBadRequest('The crew for this event is locked.')

    a.delete()
    return HttpResponseRedirect(reverse('current_season'))


@login_required
def myprofile(request):
    me: Person = Person.objects.get(user=request.user)
    ctx = {
        'me': me
    }
    if request.method == 'POST':
        f = PersonForm(request.POST, instance=me)
        if f.is_valid():
            me = f.save()
            request.user.first_name = me.display_name
            request.user.email = f["email"].value()
            request.user.user_name = request.user.email
            request.user.save()

            return HttpResponseRedirect(reverse('index'))
    else:
        f = PersonForm(instance=me, initial={'email': me.user.email})

    ctx['form'] = f

    return render(request, 'toast/account_setup.html', ctx)


@login_required
def show_event(request, eventid):
    event: Event = get_object_or_404(Event, pk=eventid)
    me: Person = Person.objects.get(user=request.user)

    # Only the relevant head official can see things. Or staff.
    i_am_crew_head_official = event.crew_heads.filter(pk=me.pk).exists()
    i_am_an_admin = event.can_delete_event(me)
    i_am_in_charge_of_the_day = i_am_an_admin or i_am_crew_head_official

    my_avail = event.availability_set.filter(person=me)
    if my_avail.exists():
        my_avail = my_avail.first()
    else:
        my_avail = None

    # League reps can see their own nominees.
    my_teams_nominees = event.availability_set.filter(nominating_league__in=me.representative_of_leagues.all()).distinct()

    event_title = f'{event.host.name} present: {event.name}' if event.name else f'{event.host.name} host in {event.city} on {event.date}'

    ctx = {
        'event': event,
        'event_title': event_title,
        'me': me,
        'i_am_in_charge': i_am_in_charge_of_the_day,
        'i_am_an_admin': i_am_an_admin,
        'my_avail': my_avail,
        'my_teams_nominees': my_teams_nominees,
        'roles': Role.objects.all(),
        'all_avails': event.availability_set.all().order_by('person__display_name'),
    }

    return render(request, 'toast/show_event.html', ctx)


@login_required
def export_event_to_csv(request, eventid):
    event: Event = get_object_or_404(Event, pk=eventid)
    me: Person = Person.objects.get(user=request.user)

    if not event.am_in_charge(me):
        return HttpResponse('Nice try. You are not a head official for this event.')

    response = HttpResponse(content_type='text/plain')

    headers = [
        'display_name',
        'affiliation',
        'fb_or_contact',
        'pronouns',
        'wftda_id',
        'history',
        'note',
    ]

    if event.is_tournament_game_day:
        headers.append('nominating_league')

    headers.extend(Role.objects.all().values_list('name', flat=True))
    headers.extend(Position.objects.all().values_list('name', flat=True))
    response.write("; ".join(headers))
    response.write("\r\n")

    for avail in event.availability_set.all().select_related('person').order_by('person'):
        response.write(f"{avail.person.display_name}; ")
        response.write(f"{avail.person.affiliation}; ")
        response.write(f"{avail.person.facebook_profile}; ")
        response.write(f"{avail.person.pronouns}; ")
        response.write(f"{avail.person.wftda_cert_claimed_identity}; ")
        response.write(f"{avail.person.games_history_link}; ")
        response.write(f"{avail.person.notes.replace(',', ' ')}; ")
        if event.is_tournament_game_day:
            response.write(f"{avail.nominating_league}; ")
        # Oh god I'm so sorry please somehow re-write this
        for r in Role.objects.all():
            response.write("Y; " if r.availability_set.filter(pk=avail.pk).exists() else "; ")
        for p in Position.objects.all():
            response.write("Y; " if p.availability_set.filter(pk=avail.pk).exists() else "; ")
        response.write("\r\n")

    return response


@login_required
def create_league(request: HttpRequest) -> HttpResponse:
    me: Person = Person.objects.get(user=request.user)

    if request.method == 'POST':
        form = CreateLeagueForm(request.POST)
        teams_form = InlineTeamsFormSet(request.POST, request.FILES)
        ctx = {
            'form': form,
            'teams_form': teams_form
        }
        if form.is_valid():
            new_name = form.cleaned_data['name']
            region_name = form.cleaned_data['region']
            region = Region.objects.filter(name=region_name).first()
            if region.league_set.filter(name__iexact=new_name).exists():
                form.add_error('name', 'This league already exists in this region.')
                return render(request, 'toast/create_league.html', ctx)
            if teams_form.is_valid():
                # Disallow duplicate leagues

                league = form.save()
                me.representative_of_leagues.add(league)

                teams_form.instance = league
                teams_form.save()
                return HttpResponseRedirect(reverse('current_season'))
    else:
        form = CreateLeagueForm()
        teams_form = InlineTeamsFormSet

        ctx = {
            'form': form,
            'teams_form': teams_form
        }
    return render(request, 'toast/create_league.html', ctx)


@login_required
def create_event(request: HttpRequest) -> HttpResponse:
    me: Person = Person.objects.get(user=request.user)
    can_create_event = me.can_create_event()
    if not (can_create_event):
        return HttpResponse("You must be a registered Representative of a League or an Administrator of a Region to create events.")

    # If I'm a league rep I can only use one of my leagues as the host
    # If I'm a region admin I can create an event for any league in my region
    possible_leagues = League.objects.filter(Q(region__in=me.admin_of_regions.all()) | Q(pk__in=me.representative_of_leagues.all())).order_by('name')
    if request.method == 'POST':
        form = CreateEventForm(request.POST)
        form.fields['host'].queryset = possible_leagues
        games_form = InlineGamesFormSet(request.POST, request.FILES)
        ctx = {
            'form': form,
            'games_form': games_form
        }
        if form.is_valid():
            if games_form.is_valid():
                me = form.save()
                games_form.instance = me
                games_form.save()
                return HttpResponseRedirect(reverse('show_event', args=[me.pk]))
    else:
        form = CreateEventForm()
        form.fields['host'].queryset = possible_leagues
        games_form = InlineGamesFormSet()
        ctx = {
            'form': form,
            'games_form': games_form
        }
    return render(request, 'toast/create_event.html', ctx)


@login_required
def del_event(request: HttpRequest, eventid: int) -> HttpResponse:
    event: Event = get_object_or_404(Event, pk=eventid)
    me: Person = Person.objects.get(user=request.user)
    i_can_delete = event.can_delete_event(me)
    if not i_can_delete:
        return HttpResponseForbidden('You may not delete this event')

    event.delete()
    return HttpResponseRedirect(reverse('current_season'))


@login_required
def edit_event(request, eventid):
    event: Event = get_object_or_404(Event, pk=eventid)
    me: Person = Person.objects.get(user=request.user)

    # I need to be admin, staff, an RHO for this event or a team rep for the host
    if not event.can_edit_basic_data(me):
        return HttpResponse('YOOOOU SHALL NOOOOT edit this event')

    ctx = {
        'event': event,
        'me': me
    }
    if request.method == 'POST':
        form = EventForm(request.POST, instance=event)
        games_form = InlineGamesFormSet(request.POST, request.FILES, instance=event)
        if form.is_valid():
            me = form.save()
            if games_form.is_valid():
                games_form.instance = me
                games_form.save()
            return HttpResponseRedirect(reverse('show_event', args=[event.pk]))
    else:
        form = EventForm(instance=event)
        games_form = InlineGamesFormSet(instance=event)

    ctx['form'] = form
    ctx['games_form'] = games_form

    return render(request, 'toast/edit_event.html', ctx)


@login_required
def your_events(request: HttpRequest):
    now = timezone.now()
    from_date = now - datetime.timedelta(days=settings.TOAST_CALENDAR_GOBACK_DAYS)

    me: Person = Person.objects.get(user=request.user)

    # Show me Tournament events I'm responsible as Region Admin or crew head!
    my_regions = me.admin_of_regions.all()
    my_tournament_events_as_region_admin_or_crew_head = Event.objects.all().filter(is_tournament_game_day=True).filter(
        Q(host__region__in=my_regions) |
        Q(crew_heads=me)
    )
    my_tournament_events_as_region_admin_or_crew_head = my_tournament_events_as_region_admin_or_crew_head.order_by('date').distinct()

    # Show me non-tournament games I've been deleted as Crew Head
    my_non_tournament_events_as_crew_head = Event.objects.all().filter(is_tournament_game_day=False).filter(crew_heads=me)

    # Show me non-Tournament events I'm responsible for as League Rep
    my_leagues = me.representative_of_leagues.all()
    my_non_tournament_events_as_league_rep = Event.objects.filter(host__in=my_leagues)
    #my_non_tournament_events_as_league_rep = Game.objects.filter(away_team__league__in=my_leagues) | Game.objects.filter(home_team__league__in=my_leagues)
    my_non_tournament_events_as_league_rep = my_non_tournament_events_as_league_rep.order_by('date')

    # Show me Tournament games I'm responsible for as Team Rep - where is my team playing
    my_leagues = me.representative_of_leagues.all()
    my_tournament_games_as_league_rep = Game.objects\
        .filter(event__is_tournament_game_day=True)\
        .filter(Q(away_team__league__in=my_leagues) | Q(home_team__league__in=my_leagues))
    my_tournament_games_as_league_rep = my_tournament_games_as_league_rep.order_by('event__date')

    if from_date:
        my_tournament_events_as_region_admin_or_crew_head = my_tournament_events_as_region_admin_or_crew_head.filter(date__gte=from_date)
        my_non_tournament_events_as_league_rep = my_non_tournament_events_as_league_rep.filter(date__gte=from_date)
        my_non_tournament_events_as_crew_head = my_non_tournament_events_as_crew_head.filter(date__gte=from_date)
        my_tournament_games_as_league_rep = my_tournament_games_as_league_rep.filter(event__date__gte=from_date)

    my_tournament_games_as_league_rep_game_to_noms_count = {
        game: game.event.availability_set.filter(nominating_league__in=my_leagues).distinct().count()
        for game in my_tournament_games_as_league_rep
    }

    ctx = {
        'me': me,
        'my_tournament_events_as_region_admin_or_crew_head': my_tournament_events_as_region_admin_or_crew_head,
        'my_non_tournament_events_as_league_rep': my_non_tournament_events_as_league_rep,
        'my_non_tournament_events_as_crew_head': my_non_tournament_events_as_crew_head,
        'my_tournament_games_as_league_rep': my_tournament_games_as_league_rep,
        'my_tournament_games_as_league_rep_game_to_noms_count': my_tournament_games_as_league_rep_game_to_noms_count
    }
    return render(request, 'toast/my_events.html', ctx)


@login_required
def your_avails(request: HttpRequest):
    now = timezone.now()
    from_date = now - datetime.timedelta(days=settings.TOAST_CALENDAR_GOBACK_DAYS)
    me: Person = Person.objects.get(user=request.user)

    # Show me availabilities I've made this season
    avails = me.availability_set.order_by('event__date')

    if from_date:
        avails = avails.filter(event__date__gte=from_date)

    ctx = {
        'me': me,
        'avails': avails
    }
    return render(request, 'toast/my_avails.html', ctx)


def transform_event_to_calendardata(instance: Event):
    # See https://fullcalendar.io/docs/event-parsing for spec
    return {
            'id': instance.pk,
            'url': reverse('show_event', args=[instance.pk]),
            'title': f'{instance.host.name} host at {instance.city}',
            'start': instance.date.isoformat(),
            'allDay': True,
            #  'end': (instance.time + timedelta(hours=instance.duration_hours)).isoformat(),
            'backgroundColor': instance.host.calendar_colour,
        }


@login_required
def current_season_cal(request):
    events = Event.season()
    payload = [
        transform_event_to_calendardata(event)
        for event in events

    ]
    return JsonResponse(payload, safe=False)


@login_required
def delete_account(request):
    return render(request, 'toast/delete_account.html')


@login_required
def really_delete_account(request):
    user = request.user
    logout(request)
    success = user.delete()
    if not success:
        return HttpResponseServerError("Couldn't delete for some reason")

    return HttpResponseRedirect(reverse('index'))


def format_ical_date_string(dt):
    # c.f. https://www.kanzaki.com/docs/ical/dateTime.html
    return dt.strftime('%Y%m%dT%H%M%SZ')


def ical(request):
    event_duration = timezone.timedelta(hours=settings.TOAST_EVENT_DURATION_HOURS)
    resp = HttpResponse(content_type='text/calendar')

    resp.write('BEGIN:VCALENDAR\r\n')
    resp.write('VERSION:2.0\r\n')
    resp.write('PRODID:-//RDEC//NONSGML v1.0//EN\r\n')
    now = timezone.now()

    then = now - datetime.timedelta(days=settings.TOAST_CALENDAR_GOBACK_DAYS)

    event_list = Event.objects.filter(date__gt=then).order_by('date')
    for e in event_list:
        end = e.datetime + event_duration

        url = reverse('show_event', args=[e.pk])
        url = request.build_absolute_uri(url)

        summary = f'{e.host.name} host in {e.city}'
        description = summary
        if e.name:
            summary = f'{e.name}'
        description = f'{description}\\n\r\n ({url})'

        resp.write('BEGIN:VEVENT\n')
        resp.write(f'UID:{e.pk}@{request.get_host()}\r\n')
        resp.write(f'DTSTAMP:{format_ical_date_string(e.last_modified)}\r\n')
        resp.write(f'DTSTART:{format_ical_date_string(e.datetime)}\r\n')
        resp.write(f'DTEND:{format_ical_date_string(end)}\r\n')
        resp.write(f'SUMMARY:{summary}\r\n')
        resp.write(f'DESCRIPTION:{description}\r\n')
        resp.write(f'LOCATION:{e.city}\r\n')
        resp.write(f'URL:{url}\r\n')

        resp.write('END:VEVENT\r\n')

    resp.write('END:VCALENDAR\r\n')
    return resp


@login_required
def league_rep_report(request: HttpRequest):
    me: Person = Person.objects.get(user=request.user)
    if not (me.user.is_staff or me.user.is_superuser):
        return HttpResponseBadRequest('Nice try.')

    leagues_with_reps = League.objects.all().order_by('name')
    return render(request, 'toast/team_rep_report.html', {'leagues': leagues_with_reps})


@login_required
def make_crew_head(request, eventid, personid):
    event: Event = get_object_or_404(Event, pk=eventid)
    me: Person = Person.objects.get(user=request.user)
    new_head: Person = get_object_or_404(Person, pk=personid)

    # I need to be an Regional Admin or League Rep
    if not event.can_delete_event(me):
        return HttpResponseBadRequest('Nice try. You are not an RHO for this event.')

    # The selected Crew Head needs to be rostered!
    # Because otherwise construction the e-mail list is just too hard.
    availability: Availability = event.availability_set.get(person__pk=new_head.pk)
    if not availability.selected_for_crew:
        return HttpResponseBadRequest("You must first Roster this person before making them a Crew Head by clicking one of the Role buttons!")

    event.crew_heads.add(new_head)

    return HttpResponse()


@login_required
def remove_crew_head(request, eventid, personid):
    event: Event = get_object_or_404(Event, pk=eventid)
    me: Person = Person.objects.get(user=request.user)
    old_head: Person = get_object_or_404(Person, pk=personid)

    # I need to be an Regional Admin or League Rep
    if not event.can_delete_event(me):
        return HttpResponseBadRequest('Nice try. You are not an RHO for this event.')

    event.crew_heads.remove(old_head)

    return HttpResponse()


@login_required
def lock_in_crew_and_email(request, eventid):
    event: Event = get_object_or_404(Event, pk=eventid)
    me: Person = Person.objects.get(user=request.user)
    if not event.am_in_charge(me):
        return HttpResponseBadRequest("You must be an RHO to perform this action")

    return event.lock_crew(request, True)




@login_required
def lock_in_crew(request, eventid):
    event: Event = get_object_or_404(Event, pk=eventid)
    me: Person = Person.objects.get(user=request.user)
    if not event.am_in_charge(me):
        return HttpResponseBadRequest("You must be an RHO to perform this action")

    return event.lock_crew(request)



@login_required
def select_official(request, availabilityid, roleid):
    availability = get_object_or_404(Availability, pk=availabilityid)
    role = get_object_or_404(Role, pk=roleid)
    event: Event = availability.event
    me: Person = Person.objects.get(user=request.user)
    i_am_in_charge = event.am_in_charge(me)
    if not i_am_in_charge:
        return HttpResponseBadRequest("You must be a League Rep, Regional Admin or CHO to perform this action")

    if event.crew_locked:
        # Too late.
        return HttpResponseBadRequest("The event's crew is already locked in.")

    availability.selected_for_crew = True
    availability.selected_for_crew_role = role

    availability.save()
    # return HttpResponseRedirect(reverse('show_event', args=[event.pk]))
    return HttpResponse()


@login_required
def unselect_official(request, availabilityid):
    availability = get_object_or_404(Availability, pk=availabilityid)
    event: Event = availability.event
    me: Person = Person.objects.get(user=request.user)
    i_am_in_charge = event.am_in_charge(me)
    if not i_am_in_charge:
        return HttpResponseBadRequest("You must be a League Rep, Regional Admin or CHO to perform this action")

    if event.crew_locked:
        # Too late.
        return HttpResponseBadRequest("The event's crew is already locked in.")

    availability.selected_for_crew = False
    availability.selected_for_crew_role = None
    event.crew_heads.remove(availability.person)

    availability.save()

    return HttpResponse()
