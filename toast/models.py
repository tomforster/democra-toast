from django.core import mail
from django.core.exceptions import ValidationError
from django.core.mail import EmailMessage
from django.db import models
from django.conf import settings
from django.db.models import Q, QuerySet
from django.http import HttpRequest, HttpResponse, HttpResponseBadRequest
from django.template import loader
from django.urls import reverse
from django.utils import timezone
from datetime import time
from django.db.models.signals import post_save
from django.dispatch import receiver

from toast.colours import colour_picker


class Association(models.Model):  # WTFDA/MRDA/JRDA/Other
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class GameType(models.Model):  # Sanc/National/Reg/Other
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Role(models.Model):  # SO/NSO
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Position(models.Model):
    class Meta:
        ordering = ["role", "name"]
    role = models.ForeignKey('Role', on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    explanatory_text = models.CharField(max_length=200, blank=True)

    def __str__(self):
        # TODO: this is somewhat verbose because the widget for the multi-select uses it in the availability form
        # would be better to do this sort of logic in the template but means writing a custom template for
        # multi-selects. We probably want to do this anyway so we can use checkboxes without JS tomfoolery
        if self.explanatory_text:
            return f'{self.name} ({self.explanatory_text})'
        return self.name


class Person(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    display_name = models.CharField(max_length=200)
    # recycle e-mail address, admin field from user model
    home_postcode = models.CharField(max_length=20, blank=True)
    affiliation = models.CharField(max_length=200, blank=True)
    facebook_profile = models.CharField(max_length=200)
    wftda_cert_claimed_identity = models.CharField(max_length=100, blank=True)
    pronouns = models.CharField(max_length=50, blank=True)
    games_history_link = models.URLField(blank=True)
    emergency_contact_details = models.CharField(max_length=200, blank=True)
    notes = models.CharField(max_length=200, blank=True)  # Medical info, etc
    opt_in_to_update_emails = models.BooleanField(default=False)

    preferred_roles = models.ManyToManyField('Role', blank=True)
    preferred_positions = models.ManyToManyField('Position', blank=True)

    def can_create_event(self) -> bool:
        # I need to be a region admin or host league rep to create:
        i_am_league_rep = self.representative_of_leagues.exists()
        i_am_region_admin = self.admin_of_regions.exists()
        return i_am_region_admin or i_am_league_rep

    def __str__(self):
        return self.display_name


class Region(models.Model):
    name = models.CharField(max_length=100)
    admins = models.ManyToManyField('Person', related_name='admin_of_regions')

    def __str__(self):
        return f'{self.name}'


class League(models.Model):
    name = models.CharField(max_length=100, db_index=True)
    region = models.ForeignKey('Region', on_delete=models.CASCADE)
    based_in_city = models.CharField(max_length=100)
    representatives = models.ManyToManyField('Person', blank=True, related_name='representative_of_leagues')
    calendar_colour = models.CharField(max_length=50, blank=True, null=True,
                                       default=colour_picker)  # A css colour name or string

    def __str__(self):
        return f'{self.name}'


class Team(models.Model):
    class Meta:
        ordering = ("league__name",)
    name = models.CharField(max_length=100, db_index=True)
    league = models.ForeignKey('League', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.league} - {self.name}'


def midday():
    return time(hour=12, minute=00)


class Event(models.Model):
    class Meta:
        ordering = ['date']

    name = models.CharField(max_length=100, blank=True)
    is_tournament_game_day = models.BooleanField(default=False)  # specifically 'the tournament this site manages'
    host = models.ForeignKey('League', on_delete=models.CASCADE)
    date = models.DateField(default=timezone.now)
    start_time = models.TimeField(default=midday)
    city = models.CharField(max_length=100)
    address = models.CharField(max_length=200, blank=True)
    schedule = models.TextField(blank=True)
    last_modified = models.DateTimeField(auto_now=True)
    facebook_event_url = models.URLField(blank=True, null=True)
    officials_group_or_event_url = models.URLField(blank=True, null=True)
    crew_heads = models.ManyToManyField('Person', related_name='crew_heading_event', blank=True)
    crew_locked = models.BooleanField(default=False)

    @property
    def friendly_name(self):
        if self.name:
            return f'{self.host.name} present: {self.name}'
        else:
            return f"{self.host.name}'s Event"

    def can_delete_event(self, person: Person) -> bool:
        # I can delete it if I'm a rep for the host league or an admin for the host league region or a sysadmin
        is_sysadmin = person.user.is_staff
        is_league_rep = person.representative_of_leagues.filter(pk=self.host.pk).exists()
        is_region_admin = person.admin_of_regions.filter(pk=self.host.region.pk).exists()
        return is_league_rep or is_region_admin or is_sysadmin

    def am_in_charge(self, person: Person) -> bool:
        i_can_delete = self.can_delete_event(person)
        i_am_head_official = self.is_head_official(person)
        return i_can_delete or i_am_head_official

    def __str__(self):
        insert = f'present {self.name}' if self.name else 'event'
        return f'{self.host} {insert} on {self.date} in {self.city}'

    @classmethod
    def season(cls) -> QuerySet:
        # We redefine season as 'past events up to x months ago and future events up to y months from now'
        today = timezone.now()
        earliest_date = today - timezone.timedelta(weeks=settings.TOAST_SEASON_PAST_HORIZON_WEEKS)
        latest_date = today - timezone.timedelta(weeks=settings.TOAST_SEASON_FUTURE_HORIZON_WEEKS)
        return cls.objects.all().filter(date__gte=earliest_date).filter(date__gte=latest_date).order_by('date')

    @property
    def datetime(self):
        return timezone.datetime.combine(self.date, self.start_time)

    def teams_query(self):
        # We want Leagues whose Teams are playing in this Event
        return League.objects.filter(Q(team__home_games__in=self.game_set.all()) | Q(team__away_games__in=self.game_set.all())).distinct()

    @property
    def noms_due_in_days(self):
        return (self.date - settings.TOAST_NOMINATION_DEADLINE_DAYSBEFORE - timezone.now().date()).days

    def interested_people(self):
        return Person.objects.filter(
            Q(availability__in=self.availability_set.all()) |
            Q(representative_of_leagues__in=self.teams_query()) |
            Q(admin_of_regions=self.host.region), opt_in_to_update_emails=True
        ).order_by('pk').distinct()

    def interested_people_email_addresses(self):
        return self.interested_people().values_list('user__email', flat=True)

    def send_changed_emails(self, request):
        # e-mail anyone who marked themselves as available for this, League Reps and RHOs.
        # IF they've opted in
        if not settings.TOAST_SEND_UPDATE_EMAILS:
            return
        my_url = request.build_absolute_uri(reverse('show_event', args=[self.pk]))
        root_url = request.build_absolute_uri(reverse('index'))

        people = self.interested_people()
        if not people.exists():
            return
        with mail.get_connection() as connection:
            for person in people:
                text = loader.render_to_string('toast/event_changed_email.txt', {'event': self, 'person': person, 'event_url': my_url, 'root_url': root_url, 'TOAST_NAME': settings.TOAST_NAME})
                email = EmailMessage(
                    f"{settings.EMAIL_SUBJECT_PREFIX} An Event you're interested in has changed",
                    text,
                    settings.DEFAULT_FROM_EMAIL,
                    [person.user.email],
                    connection=connection
                )
                email.send(fail_silently=True)

    def send_delete_emails(self, request):
        root_url = request.build_absolute_uri(reverse('index'))
        people = self.interested_people()
        if not people:
            return
        with mail.get_connection() as connection:
            for person in people:
                text = loader.render_to_string('toast/event_deleted_email.txt', {'event': self, 'person': person, 'TOAST_NAME': settings.TOAST_NAME, 'root_url': root_url})
                email = EmailMessage(
                    f"{settings.EMAIL_SUBJECT_PREFIX} An Event you're interested in has been removed",
                    text,
                    settings.DEFAULT_FROM_EMAIL,
                    [person.user.email],
                    connection=connection
                )
                email.send(fail_silently=True)

    def can_edit_basic_data(self, who: Person):
        can_edit = self.can_edit(who)
        i_am_host = self.host.representatives.filter(pk=who.pk).exists()

        return can_edit or i_am_host

    def can_edit(self, who: Person):
        i_am_a_region_admin = self.host.region.admins.filter(pk=who.pk).exists()
        return i_am_a_region_admin or who.user.is_staff or who.user.is_superuser

    def is_head_official(self, who: Person):
        i_am_a_region_admin = self.host.region.admins.filter(pk=who.pk).exists()
        i_am_crew_head = self.crew_heads.filter(pk=who.pk).exists()
        return i_am_a_region_admin or i_am_crew_head

    def lock_crew(self, request: HttpRequest, send_email: bool = False) -> HttpResponse:
        # Lock-in the event crew
        # Send e-mails to all Availabilities - success to those set marked_for_crewing, commiserations otherwise.
        # Only RHOs may do this.
        if self.crew_locked:
            # Already locked. Don't send e-mails twice!
            return HttpResponse()

        selected_officials = self.availability_set.filter(selected_for_crew=True)
        unselected_officials = self.availability_set.filter(selected_for_crew=False)

        if not selected_officials.exists():
            return HttpResponseBadRequest("You have not selected a crew yet! Click the Role Rostering button(s) next to your selections!")

        self.crew_locked = True
        self.save()

        if settings.TOAST_SEND_UPDATE_EMAILS and send_email:
            self.email_congrats_and_commiserations_from_request(request, selected_officials, unselected_officials)

        # Free up the availability for people who were not selected.
        unselected_officials.exclude(person__pk__in=self.crew_heads.all()).delete()

    def email_congrats_and_commiserations_from_request(self, request: HttpRequest, selected_officials: QuerySet, unselected_officials: QuerySet):
        my_url = request.build_absolute_uri(reverse('show_event', args=[self.pk]))
        root_url = request.build_absolute_uri(reverse('index'))
        self.email_congrats_and_commiserations(my_url, root_url, selected_officials, unselected_officials)

    def email_congrats_and_commiserations(self,
                                          my_url: str,
                                          root_url: str,
                                          selected_officials: QuerySet,
                                          unselected_officials: QuerySet):
        with mail.get_connection() as connection:
            ctx = {
                'event': self,
                'event_url': my_url,
                'root_url': root_url,
                'TOAST_NAME': settings.TOAST_NAME
            }
            for selected in selected_officials:
                ctx['person'] = selected.person
                ctx['role'] = selected.selected_for_crew_role.name
                text = loader.render_to_string('toast/event_selected_email.txt', ctx)
                email = EmailMessage(
                    f"{settings.EMAIL_SUBJECT_PREFIX} You've been rostered for an event!",
                    text,
                    settings.DEFAULT_FROM_EMAIL,
                    [selected.person.user.email],
                    connection=connection
                )
                email.send(fail_silently=True)
            for unselected in unselected_officials:
                ctx['person'] = unselected.person
                text = loader.render_to_string('toast/event_not_selected_email.txt', ctx)
                email = EmailMessage(
                    f"{settings.EMAIL_SUBJECT_PREFIX} You were not rostered for an event",
                    text,
                    settings.DEFAULT_FROM_EMAIL,
                    [unselected.person.user.email],
                    connection=connection
                )
                email.send(fail_silently=True)


class Game(models.Model):
    event = models.ForeignKey('Event', on_delete=models.CASCADE)
    home_team = models.ForeignKey('Team', on_delete=models.CASCADE, related_name='home_games')
    away_team = models.ForeignKey('Team', on_delete=models.CASCADE, related_name='away_games')
    # TODO: this isn't the correct way to add constraints, c.f.
    # TODO: https://docs.djangoproject.com/en/3.0/ref/models/options/#django.db.models.Options.constraints
    different_teams_constraint = models.UniqueConstraint(fields=['home_team', 'away_team'], name='different_teams')
    game_type = models.ForeignKey('GameType', on_delete=models.CASCADE)
    association = models.ForeignKey('Association', on_delete=models.CASCADE)

    def home_team_nom_count(self):
        return self.nom_count(self.home_team.league)

    def away_team_nom_count(self):
        return self.nom_count(self.away_team.league)

    def nom_count(self, league):
        return league.availability_set.filter(event__game=self).distinct().count()

    def teams_query(self):
        return Team.objects.filter(Q(home_games=self) | Q(away_games=self)).distinct()


class Availability(models.Model):
    class Meta:
        verbose_name_plural = "Availabilities"

    person = models.ForeignKey('Person', on_delete=models.CASCADE)
    event = models.ForeignKey('Event', on_delete=models.CASCADE)
    notes = models.CharField(max_length=200, blank=True)
    roles = models.ManyToManyField('Role', verbose_name='Role (please choose at least one)')
    preferences = models.ManyToManyField('Position', blank=True, verbose_name='Position Preferences (optional)')
    selected_for_crew = models.BooleanField(default=False)
    selected_for_crew_role = models.ForeignKey('Role', blank=True, null=True, on_delete=models.SET_NULL, related_name='selected_officials')

    nominating_league = models.ForeignKey('League', blank=True, null=True, on_delete=models.SET_NULL,
                                        verbose_name='Nominating League (please leave blank unless specifically asked by a League)')

    def validate_unique(self, *args, **kwargs):
        super().validate_unique(*args, **kwargs)
        qs = self.person.availability_set.filter(event__date=self.event.date).exclude(pk=self.pk)
        if qs.exists():
            raise ValidationError('You may not be available for more than one event one the same day')

    def __str__(self):
        return f"{self.person.display_name}'s availability for {self.event}"


class Settings(models.Model):
    class Meta:
        verbose_name_plural = "Settings"

    # A singleton model.
    def save(self, *args, **kwargs):
        self.pk = 1
        super(Settings, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_person_object(sender, instance, created, raw, using, update_fields, **kwargs):
    if not created:
        return
    if raw:
        return
    user = instance
    p = Person.objects.create(user=user, display_name=user.first_name or 'No Name')

