(function() {
    let rosterButtons = document.querySelectorAll('.js-roster-button--enroster');
    let unRosterButtons = document.querySelectorAll('.js-roster-button--unselect');
    let crewHeadButtons = document.querySelectorAll('.js-roster-button--head');

    if (rosterButtons) {
        for (let i = 0; i < rosterButtons.length; i++) {
            setupButtonStyleByDataElement(rosterButtons[i], "data-iscrewed");
            rosterButtons[i].addEventListener('click', enRoster);
        }
    }

    if (crewHeadButtons) {
        for (let j = 0; j < crewHeadButtons.length; j++) {
            setupButtonStyleByDataElement(crewHeadButtons[j], 'data-ishead');
            crewHeadButtons[j].addEventListener('click', toggleCrewHead);
        }
    }

    if (unRosterButtons) {
        for (let k = 0; k < unRosterButtons.length; k++) {
            unRosterButtons[k].addEventListener('click', unRoster);
        }
    }
})();

function setupButtonStyleByDataElement(btn, switchOnDataElementName) {
    let isSelected = (btn.getAttribute(switchOnDataElementName) === "true");
    setupButtonStyle(btn, isSelected);
}

function setupButtonStyle(btn, isSelected){
    let selectedClasses = ['btn-primary'];
    let unSelectedClasses = ['btn-outline-primary'];

    let selectedState = {
        addClasses: selectedClasses,
        removeClasses: unSelectedClasses
    };
    let unselectedState = {
        addClasses: unSelectedClasses,
        removeClasses: selectedClasses
    };

    let state = isSelected ? selectedState : unselectedState;
    btn.classList.add(state.addClasses);
    btn.classList.remove(state.removeClasses);

}

function enRoster(e) {
    let encrewUrl = this.getAttribute('data-encrewurl');
    let isAlreadyCrewed = (this.getAttribute('data-iscrewed') === "true");
    if (isAlreadyCrewed) {
        return;
    }

    fetch(encrewUrl).then(response => {
        if (response.status === 200) {

            // Unselect all role buttons
            delesectRosterButtons(this, 'data-iscrewed');

            // select current role button
            this.setAttribute('data-iscrewed', "true");
            setupButtonStyle(this, true);
        } else {
            bootbox.alert("There has been an error. Please reload the page.");
        }
    });
}

function unRoster(event) {
    let decrewUrl = this.getAttribute('data-decrewurl');

    fetch(decrewUrl).then(response => {
        if (response.status === 200) {

            // Unselect all role buttons
            delesectRosterButtons(this, 'data-iscrewed');
            delesectCrewHeadButtons(this, 'data-ishead');
        } else {
            bootbox.alert("There has been an error. Please reload the page.");
        }
    });
}

function delesectCrewHeadButtons(nearestButton, isSelectedDataElement) {
    let buttonContainer = nearestButton.closest('.js-roster-buttons');
    let buttons = buttonContainer.querySelectorAll('.js-roster-button:not(.js-roster-button--unselect):not(.js-roster-button--enroster)');
    for (var j = 0; j < buttons.length; j++) {
        let btn = buttons[j];
        btn.setAttribute(isSelectedDataElement,"false");
        setupButtonStyle(btn, false);
    }
}

function delesectRosterButtons(nearestButton, isSelectedDataElement) {
    let buttonContainer = nearestButton.closest('.js-roster-buttons');
    let buttons = buttonContainer.querySelectorAll('.js-roster-button:not(.js-roster-button--unselect):not(.js-roster-button--head)');
    for (var j = 0; j < buttons.length; j++) {
        let btn = buttons[j];
        btn.setAttribute(isSelectedDataElement,"false");
        setupButtonStyle(btn, false);
    }
}

function toggleCrewHead(e) {
    let promoteUrl = this.getAttribute('data-promoteurl');
    let demoteUrl = this.getAttribute('data-demoteurl');
    let isAlreadyHead = (this.getAttribute('data-ishead') === "true");

    // If I am now a Head: this button will demote me
    let url = isAlreadyHead ? demoteUrl : promoteUrl;

    fetch(url).then(response => {
        if (response.status === 200) {
            // Invert my current state!
            this.setAttribute('data-ishead', isAlreadyHead ? "false" : "true");
            setupButtonStyle(this, !isAlreadyHead);
        } else {

            var text = response.text().then((text) => {
                if (text) {
                    bootbox.alert(text);
                } else {
                    bootbox.alert("There has been an error. Please reload the page.");
                }
            });

        }
    });
}
