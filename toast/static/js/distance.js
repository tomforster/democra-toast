function initMap() {
    //kill for now
    return false;
    
    if (storage) {
        var lookups = document.querySelectorAll('.js-distance');

        if (lookups) {
            for (var j = 0; j < lookups.length; j++) {
                var el = lookups[j];
                var from = el.getAttribute('data-from');
                var to = el.getAttribute('data-to');
                var distance;

                if (from !== "" && to !== "") {
                    distance = getDistanceFromCache(from,to);
                    if (distance === null) {
                        distance = performLookup(from, to, el)
                    } else {
                        el.innerHTML = distance.miles + ' / ' + distance.duration;
                    }                
                }
            }
        }
    }
}

function performLookup(from, to, el) {
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
    {
        origins: [from + ", England"],
        destinations: [to + ", England"],
        travelMode: 'DRIVING',
        unitSystem: google.maps.UnitSystem.IMPERIAL
    }, (function() {
        return function(response, status) {
            var data = {
                miles: response.rows[0].elements[0].distance.text,
                duration: response.rows[0].elements[0].duration.text
            }
            var cacheName = from + '%' + to;
            localStorage.setItem(cacheName, JSON.stringify(data));
            el.innerHTML = data.miles + ' / ' + data.duration;
        }                    
    })());
}

function getDistanceFromCache(from, to) {
    var cacheName = from + '%' + to;
    return JSON.parse(localStorage.getItem(cacheName));
}

var storage = (function() {
	var uid = new Date;
	var storage;
	var result;
	try {
		(storage = window.localStorage).setItem(uid, uid);
		result = storage.getItem(uid) == uid;
		storage.removeItem(uid);
		return result && storage;
	} catch (exception) {}
}());