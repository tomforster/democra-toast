from rest_framework import serializers

from toast.models import Event, League, Association, Region, Person, Availability, Role, \
    Position, Game, GameType, Team


class PersonSerialiser(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Person
        fields = (
            'display_name',
        )


class AssociationSerialiser(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Association
        fields = (
            'url',
            'name',
        )


class RegionSerialiser(serializers.HyperlinkedModelSerializer):
    heads = PersonSerialiser(many=True, read_only=True)

    class Meta:
        model = Region
        fields = (
            'url',
            'name',
            'admins',
        )


class LeagueSerialiser(serializers.HyperlinkedModelSerializer):
    representatives = PersonSerialiser(many=True, read_only=True)

    class Meta:
        model = League
        fields = (
            'url',
            'name',
            'region',
            'based_in_city',
            'representatives',
            'team_set'
        )


class TeamSerialiser(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Team
        fields = (
            'url',
            'league',
            'name',
        )


class EventSerialiser(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Event
        fields = (
            'url',
            'host',
            'date',
            'start_time',
            'city',
            'address',
            'schedule',
            'last_modified',
            'facebook_event_url',
            'officials_group_or_event_url',
            'crew_locked',
            'game_set'
            )


class RoleSerialiser(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Role
        fields = (
            'url',
            'name',
        )


class PositionSerialiser(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Position
        fields = (
            'url',
            'role',
            'name',
            'explanatory_text'
        )


class GameTypeSerialiser(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = GameType
        fields = (
            'name',
        )


class GameSerialiser(serializers.HyperlinkedModelSerializer):
    game_type = GameTypeSerialiser(read_only=True)

    class Meta:
        model = Game
        fields = (
            'url',
            'event',
            'game_type',
            'home_team',
            'away_team'
        )


class AvailabilitySerialiser(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Availability
        fields = (
            'url',
            'event',
            'notes',
            'roles',
            'preferences',
        )
