from django.contrib import admin
from django.contrib.admin import AdminSite, ModelAdmin, TabularInline
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.auth.models import Group

from toast.models import Association, GameType, Role, Position, Person, Region, League, Event, \
    Game, Availability, Settings, Team


class ToastAdminSite(AdminSite):
    site_header = f'{settings.TOAST_NAME} TOAST Administration'
    site_title = f'{settings.TOAST_NAME} TOAST Administration'
    index_title = f'{settings.TOAST_NAME} TOAST Administration'

class TeamInline(admin.TabularInline):
    model = Team

class LeagueAdmin(ModelAdmin):
    list_select_related = True
    search_fields = ('name', )
    ordering = ('name', )

    list_filter = ['name', ]

    inlines = [
        TeamInline
    ]


class TeamAdmin(ModelAdmin):
    list_select_related = True
    search_fields = ('name', 'league' )
    ordering = ('league', 'name', )

    list_filter = ['league', ]


class PersonAdmin(ModelAdmin):
    list_select_related = True
    search_fields = ('display_name',)
    ordering = ('display_name', )


class GameInline(admin.TabularInline):
    model = Game

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'game_type':
            kwargs['initial'] = GameType.objects.get(name='Regulation')
        if db_field.name == 'association':
            kwargs['initial'] = Association.objects.get(name='WFTDA')
        return super(GameInline, self).formfield_for_foreignkey(
            db_field, request, **kwargs
        )


class EventAdmin(ModelAdmin):
    search_fields = ('name', 'host__name', 'city')
    list_filter = ['host']
    inlines = [
        GameInline
    ]

    def save_model(self, request, obj, form, change):
        if change:
            obj.send_changed_emails(request)
        super().save_model(request, obj, form, change)

    def delete_model(self, request, obj):
        obj.send_delete_emails(request)
        super().delete_model(request, obj)


class GameAdmin(ModelAdmin):
    search_fields = ('event__host__name', 'home_team__name', 'away_team__name')


class RegionAdmin(ModelAdmin):
    search_fields = ('name',)
    list_filter = ['name']


class AvailabilityAdmin(ModelAdmin):
    search_fields = ('event__host__name', 'person__display_name',)
    ordering = ('event__date', )

    list_filter = ['event']


admin_site = ToastAdminSite(name='toast_admin')
admin_site.register(get_user_model(), UserAdmin)
admin_site.register(Group, GroupAdmin)
admin_site.register(Association)
admin_site.register(GameType)
admin_site.register(Role)
admin_site.register(Position)
admin_site.register(Person, PersonAdmin)
admin_site.register(Region, RegionAdmin)
admin_site.register(League, LeagueAdmin)
admin_site.register(Team, TeamAdmin)
admin_site.register(Event, EventAdmin)
admin_site.register(Game, GameAdmin)
admin_site.register(Availability, AvailabilityAdmin)
admin_site.register(Settings)


