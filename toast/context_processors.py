from django.conf import settings


def toast_name(request):
    return {'TOAST_NAME': settings.TOAST_NAME}
